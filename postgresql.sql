CREATE TABLE tbl_customer (
id BIGINT NOT NULL,
tin VARCHAR NOT NULL
);

insert into tbl_customer
(id, tin)
select
 1, 'customer1'
union all
select
 2, 'customer2';

CREATE EXTENSION IF NOT EXISTS tablefunc;

CREATE TABLE tbl_loan_transaction (
customer_id BIGINT NOT NULL,
type VARCHAR NOT NULL,
amount NUMERIC(10,2) NOT NULL
);

insert into tbl_loan_transaction
(customer_id, type, amount)
values
(1, 'loan', 1000.50),
(1, 'interest', 1.50),
(1, 'interest_repayment', 1.50),
(1, 'loan', 7800.00),
(2, 'loan', 5200.30),
(2, 'loan_repayment', 200),
(2, 'interest', 500.17);


--here is the result query
SELECT c.tin,
       sum(coalesce(loan, 0) - (coalesce(loan_repayment, 0)) + (coalesce(interest, 0)) - (coalesce(interest_repayment, 0))) OVER (partition by customer_id) as profile,
       round((sum(coalesce(loan, 0) - (coalesce(loan_repayment, 0)) + (coalesce(interest, 0)) - (coalesce(interest_repayment, 0))) OVER (partition by customer_id) /
       sum(coalesce(loan, 0) - (coalesce(loan_repayment, 0)) + (coalesce(interest, 0)) - (coalesce(interest_repayment, 0)))  OVER ()) * 100, 2) as "%"
FROM crosstab(
  'SELECT customer_id, type, sum(amount)
   FROM tbl_loan_transaction
   GROUP BY customer_id, type
   ORDER BY 1',
   'SELECT DISTINCT type FROM tbl_loan_transaction ORDER BY 1')
AS
(
       customer_id BIGINT,
       interest NUMERIC(10,2),
       interest_repayment NUMERIC(10,2),
       loan NUMERIC(10,2),
       loan_repayment NUMERIC(10,2)
) JOIN tbl_customer c
       ON customer_id = c.id

-- output:
--|   tin    | profile |   %  |
--|customer1 | 8800.50 | 61.54|
--|customer2 | 5500.47 | 38.46|
