<?php

require_once "Money.php";

/**
 * @param int $amount
 *
 * @return Money
 */
function Money($amount)
{
    return new Money($amount);
}

$a = Money(10) ->add (Money(20)) -> subtract (Money(5));
$b = Money(40) ->subtract ($a -> subtract(Money(3)));
$c = Money(15)->add(Money(5))->subtract(Money(4))->add(Money(12))->add($a)->subtract($b);
echo $a->asFloat() . PHP_EOL; // 25
echo $a->describe() . PHP_EOL; // ((10 + 20) – 5)
echo $b->asFloat() . PHP_EOL; // 18
echo $b->describe() . PHP_EOL; // (40 – (((10 + 20) - 5) – 3))
echo $c->asFloat() . PHP_EOL;  //35
echo $c->describe() . PHP_EOL; // (((((15 + 5) - 4) + 12) + ((10 + 20) - 5)) - (40 - (((10 + 20) - 5) - 3)))
