<?php

class Money
{
    /** @var int */
    protected $amount = 0;

    /** @var string */
    protected $description;

    /**
     * @param int    $amount
     * @param string $description
     */
    public function __construct($amount, $description = null)
    {
        $this->amount = $amount;
        $this->description = empty($description) ? $amount : $description;
    }

    public function add(Money $money)
    {
        return $this->createChild(
            $money,
            $this->amount + $money->asFloat(),
            '+'
        );
    }

    public function subtract(Money $money)
    {
        return $this->createChild(
            $money,
            $this->amount - $money->asFloat(),
            '-'
        );
    }

    public function asFloat()
    {
        return $this->amount;
    }

    public function describe()
    {
        return $this->description;
    }

    /**
     * @param Money  $money
     * @param int    $amount
     * @param string $operand
     *
     * @return Money
     */
    protected function createChild(Money $money, $amount, $operand)
    {
        return new Money(
            $amount,
            $this->prepareDescription($this->describe(), $money->describe(), $operand)
        );
    }

    /**
     * @param string $operandFirst
     * @param string $operandSecond
     * @param string $operator
     *
     * @return string
     */
    protected function prepareDescription($operandFirst, $operandSecond, $operator)
    {
        return '(' . $operandFirst . ' ' . $operator . ' ' . $operandSecond . ')';
    }
}